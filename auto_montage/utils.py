import logging
import sys
from typing import Optional

import cv2
import numpy as np
from PIL import Image


def load_from_fname(fname: str, resize: Optional[float]) -> np.ndarray:
    """
    loads image and gets rid of any extra unused dimensions
    as they smeetimes save as rgb accidently
    """
    im = Image.open(fname)
    if len(im.split()) > 1:
        im = im.split()[0]
    im = np.array(im.getdata(), dtype=np.uint8).reshape(im.size[1], im.size[0])
    if resize is not None:
        h = int(im.shape[0] * resize)
        w = int(im.shape[1] * resize)
        im = cv2.resize(im, (w, h))
    return im  # type: ignore


def configure_logging() -> None:
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
