from typing import Dict, Optional, Tuple

import numpy as np

from auto_montage.features import compute_kps_desc
from auto_montage.utils import load_from_fname


class MultiModalImage:
    index = {"split": 0, "confocal": 1, "avg": 2}

    def __init__(
        self,
        confocal: str,
        split: str,
        avg: str,
        nominal_position: np.ndarray,
        fov: float,
        resize: Optional[float],
    ):
        """
        store names, nominal position and images as a single
        numpy tensor [height, width, channel]
        """
        self.fov = fov
        self.split_fname = split
        self.confocal_fname = confocal
        self.avg_fname = avg
        split_arr = load_from_fname(split, resize)
        confocal_arr = load_from_fname(confocal, resize)
        avg_arr = load_from_fname(avg, resize)
        self.multimodal_im: np.ndarray = np.stack(
            [split_arr, confocal_arr, avg_arr], axis=2
        )
        self.nominal_position = nominal_position

        self.keypoints: Dict[str, Optional[np.ndarray]] = {
            "split": None,
            "confocal": None,
            "avg": None,
        }
        self.descriptors: Dict[str, Optional[np.ndarray]] = {
            "split": None,
            "confocal": None,
            "avg": None,
        }

    def get_confocal(
        self,
    ) -> np.ndarray:
        return self.multimodal_im[:, :, MultiModalImage.index["confocal"]]

    def get_split(
        self,
    ) -> np.ndarray:
        return self.multimodal_im[:, :, MultiModalImage.index["split"]]

    def get_avg(
        self,
    ) -> np.ndarray:
        return self.multimodal_im[:, :, MultiModalImage.index["avg"]]

    def get_split_name(
        self,
    ) -> str:
        return self.split_fname

    def get_confocal_name(
        self,
    ) -> str:
        return self.confocal_fname

    def get_avg_name(
        self,
    ) -> str:
        return self.avg_fname

    def get_nominal(
        self,
    ) -> np.ndarray:
        return self.nominal_position

    def get_image_and_name(self, mntge_type: str) -> Tuple[np.ndarray, str]:
        if mntge_type == "confocal":
            src_img = self.get_confocal()
            src_name = self.get_confocal_name()
        elif mntge_type == "split":
            src_img = self.get_split()
            src_name = self.get_split_name()
        elif mntge_type == "avg":
            src_img = self.get_avg()
            src_name = self.get_avg_name()
        else:
            raise ValueError("No type named {}".format(mntge_type))
        return src_img, src_name

    def calculate_orb(
        self,
    ) -> None:
        """calculate and set the descriptors"""
        for modality in self.keypoints.keys():
            image = self.multimodal_im[:, :, MultiModalImage.index[modality]]
            kps, desc = compute_kps_desc(image)
            self.keypoints[modality] = kps
            self.descriptors[modality] = desc
