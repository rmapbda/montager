import queue
import tkinter as tk
from threading import Event, Thread
from tkinter import messagebox
from tkinter.filedialog import askdirectory, askopenfilename
from typing import Dict, Tuple

from auto_montage.auto_montage import main


class GUI:
    def __init__(self) -> None:
        self.root = tk.Tk()
        self.frame = tk.Frame(self.root)
        self.frame.pack()
        self.welcome_window()

    def restart_frame(self) -> tk.Frame:
        self.frame.destroy()
        self.frame = tk.Frame(self.root)
        self.frame.pack()
        new_frame = tk.Frame(self.frame)
        new_frame.pack()
        return new_frame

    def welcome_window(self) -> None:
        welcome_frame = self.restart_frame()

        directory_var = tk.StringVar()
        photoshop_var = tk.StringVar()
        nominal_var = tk.StringVar()

        def get_directory() -> None:
            directory_var.set(askdirectory(title="Choose directory"))

        def get_nominal() -> None:
            nominal_var.set(askopenfilename(title="Choose nominal pos"))

        def get_photoshop() -> None:
            photoshop_var.set(askdirectory(title="Choose directory"))

        choose_directory_label = tk.Label(welcome_frame, text="Choose image folder")
        choose_directory_label.grid(row=0, column=0, sticky=tk.W)
        choose_directory = tk.Button(
            welcome_frame, text="Choose", command=get_directory
        )
        choose_directory.grid(row=0, column=1, sticky=tk.NSEW)
        choose_nominal_label = tk.Label(welcome_frame, text="Choose nominal positions")
        choose_nominal_label.grid(row=1, column=0, sticky=tk.W)
        choose_nominal = tk.Button(welcome_frame, text="Choose", command=get_nominal)
        choose_nominal.grid(row=1, column=1, sticky=tk.NSEW)

        eye = tk.StringVar()
        eye.set("None")
        eye_label = tk.Label(welcome_frame, text="Choose eye")
        eye_label.grid(row=2, column=0, sticky=tk.W)
        eye_menu = tk.OptionMenu(welcome_frame, eye, *["OD", "OS"])
        eye_menu.grid(row=2, column=1, sticky=tk.NSEW)

        conf_var = tk.StringVar()
        conf_var.set("confocal")
        conf_label = tk.Label(welcome_frame, text="confocal naming")
        conf_label.grid(row=3, column=0, sticky=tk.W)
        conf_entry = tk.Entry(welcome_frame, textvariable=conf_var)
        conf_entry.grid(row=3, column=1, sticky=tk.NSEW)

        split_var = tk.StringVar()
        split_var.set("split_det")
        split_label = tk.Label(welcome_frame, text="split naming")
        split_label.grid(row=4, column=0, sticky=tk.W)
        split_entry = tk.Entry(welcome_frame, textvariable=split_var)
        split_entry.grid(row=4, column=1, sticky=tk.NSEW)

        avg_var = tk.StringVar()
        avg_var.set("avg")
        avg_label = tk.Label(welcome_frame, text="average naming")
        avg_label.grid(row=5, column=0, sticky=tk.W)
        avg_entry = tk.Entry(welcome_frame, textvariable=avg_var)
        avg_entry.grid(row=5, column=1, sticky=tk.NSEW)

        photo_label = tk.Label(
            welcome_frame, text="Choose output photoshop script folder"
        )
        photo_label.grid(row=6, column=0, sticky=tk.W)
        photo = tk.Button(welcome_frame, text="Choose", command=get_photoshop)
        photo.grid(row=6, column=1, sticky=tk.NSEW)

        def go_cmd() -> None:
            naming = {
                "confocal": conf_var.get(),
                "split": split_var.get(),
                "avg": avg_var.get(),
            }
            self.start_montage(
                directory_var.get(),
                nominal_var.get(),
                eye.get(),
                naming,
                photoshop_var.get(),
            )

        go = tk.Button(welcome_frame, text="Montage", command=go_cmd)
        go.grid(row=7, column=0, columnspan=2, sticky=tk.NSEW)

    def start_montage(
        self,
        directory: str,
        nominal: str,
        eye: str,
        naming: Dict[str, str],
        photoshop_directory: str,
    ) -> None:
        q: queue.Queue[Tuple[int, int, int, float]] = queue.Queue()
        e = Event()
        t = Thread(
            target=main,
            args=(directory, nominal, eye, naming, photoshop_directory, q, e),
        )
        t.start()

        self.monitor_montage(q, e)

    def monitor_montage(self, q: queue.Queue, e: Event) -> None:
        monitor_frame = self.restart_frame()
        label = tk.Label(
            monitor_frame,
            text="Collecting images, can take a minute, gui hasnt frozen!",
        )
        label.grid(row=0, column=0)
        label = tk.Label(
            monitor_frame, text="Number images montaged will appear below:"
        )
        label.grid(row=1, column=0)

        def update_vars() -> None:
            try:
                done, total, fov_id, fov_name = q.get(block=False)
                label = tk.Label(
                    monitor_frame, text="{} fov: {}/{}".format(fov_name, done, total)
                )
                label.grid(row=2 + fov_id, column=1)
            except queue.Empty:
                pass
            repeat_process()

        def repeat_process() -> None:
            if e.is_set():
                messagebox.showinfo("Finished", "Finished montaging", icon="warning")
                self.welcome_window()
            else:
                self.root.after(500, update_vars)

        update_vars()
