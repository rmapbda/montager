import logging

from auto_montage.gui import GUI
from auto_montage.utils import configure_logging

logger = logging.getLogger(__name__)


def entry() -> None:
    configure_logging()
    logger.info("Starting GUI")
    g = GUI()
    g.root.mainloop()
    logger.info("Finished: Closing GUI")
