import logging
import queue
from threading import Event
from time import time
from typing import Dict, Tuple

from auto_montage.input_pipeline import InputPipeline
from auto_montage.montage_builder import MontageBuilder
from auto_montage.script_maker import write_photoshop_script
from auto_montage.transformation_finder import TransformationFinder

logger = logging.getLogger(__name__)


def main(
    directory: str,
    nominal: str,
    eye: str,
    naming: Dict[str, str],
    photoshop_directory: str,
    q: queue.Queue[Tuple[int, int, int, float]],
    e: Event,
) -> None:

    logger.info(directory)
    alg_start = time()
    # gets all our files matched with different modalities
    logger.info("Getting all files ...")
    m = InputPipeline(directory, nominal, naming, eye)
    mmList = m.as_multi_modal_objects()

    # calculates all keypoints and descriptors
    # then constructs a global registration out
    # of pairwise registrations
    for i, fov in enumerate(mmList):
        s = time()
        logger.info("Computing keypoints and descriptors for {} fov...".format(fov))
        tf = TransformationFinder(mmList[fov])
        tf.compute_kps_desc()

        logger.info("Building registrations for {} fov...".format(fov))
        tf.compute_pairwise_registrations(q, i, fov)

        logger.info("Finished {} fov!".format(fov))
        logger.info("took {}".format(time() - s))

        # list of lists. The top layer is disjoint
        # montages, followed by the transformations
        # and file names neededd
        mb = MontageBuilder(tf, evaluate=False)
        disjoint_montages = mb.construct_all_montages()

        logger.info("Creating photoshop script ...")
        transformations = [x[0] for x in disjoint_montages]
        name = "create_recent_montage_" + str(fov) + "_fov"
        write_photoshop_script(transformations, photoshop_directory, naming, name=name)
    e.set()
    logger.info("Total time taken {}".format(time() - alg_start))
