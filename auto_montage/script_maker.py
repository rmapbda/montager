import os
from io import TextIOWrapper
from typing import TYPE_CHECKING, Dict, List, Optional

if TYPE_CHECKING:
    from auto_montage.montage_builder import TransformDict


def create_preferences(jsfile: TextIOWrapper) -> None:
    jsfile.write(
        """
app.preferences.rulerUnits = Units.PIXELS
app.preferences.typeUnits = TypeUnits.PIXELS
app.displayDialogs = DialogModes.NO
var color = new SolidColor()
color.gray.gray = 100
"""
    )


def create_function(
    jsfile: TextIOWrapper, avg_name: str, split_name: str, confocal_name: str
) -> None:
    jsfile.write(
        f"""
var AVG = 0
var SPLIT = 1
var CONF = 2
var names = ["{avg_name}", "{split_name}", "{confocal_name}"]

function addLinkedImage(confocal_fname, dx, dy, h, w, doc) {{
	
	var docRef = docs[doc]
	var split_fname = confocal_fname.replace(names[CONF], names[SPLIT])
	var avg_fname = confocal_fname.replace(names[CONF], names[AVG])
	
	var file_names = [avg_fname, split_fname, confocal_fname]

	// input the three image types into there layersets
	for (var imageType = 0; imageType < 3; imageType++){{
		// open new document with just the image so we can duplicate to montage
		var img = new File(file_names[imageType]);
		var opened = open(img);
		opened.resizeImage(w, h)
        opened.resizeCanvas(w, h)
		var pSourceDocument = app.activeDocument;
		pSourceDocument.artLayers[0].duplicate(docRef);
		pSourceDocument.close(SaveOptions.DONOTSAVECHANGES)
		
		// get the layer in the montage document
		var layerInOrig = docRef.artLayers[0]
		var image_name = file_names[imageType].split('\\\\')
		layerInOrig.name = image_name[image_name.length - 1]
		layerInOrig.move(docRef.layerSets.getByName(names[imageType]),  ElementPlacement.INSIDE)
	}}
	
	// link the three images
	for (var imageType = 0; imageType < 2; imageType++) {{
		var layersetOne = docRef.layerSets.getByName(names[imageType])
		var layersetTwo = docRef.layerSets.getByName(names[imageType + 1])
		var image_one_name = file_names[imageType].split('\\\\')
		var artLayerOne = layersetOne.artLayers.getByName(image_one_name[image_one_name.length - 1])
		var image_two_name = file_names[imageType + 1].split('\\\\')
		var artLayerTwo = layersetTwo.artLayers.getByName(image_two_name[image_two_name.length - 1])
		
		artLayerOne.link(artLayerTwo)
		layersetOne.visible = false
	}}
	
	// add resizing to canvas
	var confocal_layer_set = docRef.layerSets.getByName(names[CONF])
	var confocal_name = file_names[CONF].split('\\\\')
	var confocal_image_layer = confocal_layer_set.artLayers.getByName(confocal_name[confocal_name.length - 1])
	confocal_image_layer.translate(dx, dy)
}}

for (var disjoint = 0; disjoint<docs.length; disjoint++){{
	var translations = data[disjoint]
	app.activeDocument = docs[disjoint]
	for (var i = 0; i<translations.length; i++) {{
		var d = translations[i]
		var confocal_name = d[0]
		var ty = d[1]
		var tx = d[2]
		var h = d[3]
		var w = d[4]
		addLinkedImage(confocal_name, ty, tx, h, w, disjoint)
	}}
	docs[disjoint].revealAll()
}}
"""
    )


def get_doc_add_layerset(
    doc: int, avg_name: str, split_name: str, confocal_name: str
) -> str:
    return (
        """
app.activeDocument = docs["""
        + str(doc)
        + """]
app.activeDocument.selection.fill(color, ColorBlendMode.NORMAL, 100, false)
var docRef = docs["""
        + str(doc)
        + f"""]
var confocal = docRef.layerSets.add()
confocal.name = "{confocal_name}"
var split = docRef.layerSets.add()
split.name = "{split_name}"
var avg = docRef.layerSets.add()
avg.name = "{avg_name}"
"""
    )


def create_doc(doc: int) -> str:
    return (
        'app.documents.add(10000, 10000, 72, "montage'
        + str(doc)
        + '", NewDocumentMode.GRAYSCALE)'
    )


def create_transformations(
    jsfile: TextIOWrapper, disjoint_montage: List[List["TransformDict"]]
) -> int:
    for disjoint in range(len(disjoint_montage)):
        jsfile.write("var data" + str(disjoint) + " = [")

        trans_for_join = []
        for transformation in disjoint_montage[disjoint]:
            confocal_name = transformation["confocal"]
            ty = float(transformation["transy"])
            tx = float(transformation["transx"])

            # if global_ref set its height
            if tx == 0.0 and ty == 0.0:
                global_height = int(transformation["h"])
                global_width = int(transformation["w"])

            height = int(transformation["h"])
            width = int(transformation["w"])
            trans_for_join.append(
                str(
                    [
                        confocal_name,
                        float(ty) + (width - global_width) / 2.0,
                        float(tx) + (height - global_height) / 2.0,
                        height,
                        width,
                    ]
                )
            )

        data = ",".join(trans_for_join)
        jsfile.write(data)
        jsfile.write("]\n")
    return len(disjoint_montage)


def create_array_of_data(jsfile: TextIOWrapper, chunks: int) -> None:
    jsfile.write("var data = [")
    montage_js = []
    for disjoint_montage in range(chunks):
        montage_js.append("data" + str(disjoint_montage))
    montage_js_as_string = ",".join(montage_js)
    jsfile.write(montage_js_as_string)
    jsfile.write("]\n")


def create_doc_array(jsfile: TextIOWrapper, chunks: int) -> None:
    jsfile.write("var docs = [")
    docs_js = []
    for doc in range(chunks):
        docs_js.append(create_doc(doc))
    docs_js_as_string = ",".join(docs_js)
    jsfile.write(docs_js_as_string)
    jsfile.write("]\n")


def create_layer_sets(
    jsfile: TextIOWrapper,
    chunks: int,
    avg_name: str,
    split_name: str,
    confocal_name: str,
) -> None:
    for doc in range(chunks):
        jsfile.write(get_doc_add_layerset(doc, avg_name, split_name, confocal_name))


def write_photoshop_script(
    disjoint_montages: List[List["TransformDict"]],
    photoshop_directory: str,
    naming: Dict[str, str],
    name: Optional[str] = None,
) -> None:
    fname = "create_recent_montage.jsx" if name is None else name + ".jsx"
    avg_name = naming["avg"]
    confocal_name = naming["confocal"]
    split_name = naming["split"]
    with open(os.path.join(photoshop_directory, fname), "w") as jsfile:
        # preferences, units etc
        create_preferences(jsfile)

        # creating arrays with the translations and file names
        # will have to find the right folder
        chunks = create_transformations(jsfile, disjoint_montages)

        # puts the previous arrays into a single array
        create_array_of_data(jsfile, chunks)

        # creates required number of docs
        create_doc_array(jsfile, chunks)

        # add the layersets to these docs
        create_layer_sets(jsfile, chunks, avg_name, split_name, confocal_name)

        # write the ending script which has the function
        create_function(jsfile, avg_name, split_name, confocal_name)
